<div class="sliding-door">
  <?php if (!empty($admin)) : ?>
    <div class="admin"><?php print $admin;?></div>
  <?php endif; ?>
  <?php if (!empty($contact)) : ?>
    <div class="contact"><?php print $contact;?></div>
  <?php endif; ?>
  <?php if (!empty($subject)) : ?>
    <div class="subject"><?php print $subject;?></div>
  <?php endif; ?>

  <div class="comment"><?php print $comment;?><?php print $filter;?></div>
  <?php print $rest;?>

  <div class="buttons"><?php print $buttons;?></div>
</div>