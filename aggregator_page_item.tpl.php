<div class="article feed-item teaser">
<div class="sliding-door">
  <h3 class="title"><a href="<?php print $link ?>" title="<?php print $title ?>"><?php print $title ?></a></h3>

  <?php if ($categories): ?>
    <div class="tags sliding-door">
      <div class="tags categories"><?php print t('Categories')  ?>: <?php print implode(', ', $categories)?></div>
      <?php print $terms; ?>
    </div>
  <?php endif; ?>

  <?php if ($description) : ?>
    <div class="body">
      <?php print $description ?>
    </div>
  <?php endif; ?>

  <div class="links sliding-door">
  <ul class="links">
    <li><?php print $source ?></li>
    <li class="date"><?php print $source_date ?></li>
  </ul>
  </div>

</div>
</div>