<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
  <title><?php print $head_title ?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>

<body>
<div id="container">
  <div id="branding">
    <?php if ($logo) : ?>
      <a href="<?php print url() ?>" title="<?php t('Index Page') ?>"><img src="<?php print($logo) ?>" alt="Logo" id="branding-logo"/></a>
    <?php endif; ?>

    <?php if ($site_name) : ?>
    <h1 id="branding-name"><a href="<?php print url() ?>" title="<?php t('Index Page') ?>"><?php print($site_name) ?></a></h1>
    <?php endif;?>

    <?php if ($site_slogan) : ?>
    <span id="branding-tagline"><?php print($site_slogan) ?></span>
    <?php endif;?>

    <?php print $branding ?>
  </div>

  <div id="content">
    <?php if (!empty($tabs)) print $tabs ?>

    <div id="content-main">
      <?php if (!empty($title)): ?>
        <h2 class="content title" id="content-title"><?php print $title ?></h2>
      <?php endif; ?>

      <?php if ($content_prefix): ?>
        <div id="content-prefix" class="content prefix"><?php print $content_prefix ?></div>
      <?php endif; ?>

      <?php if (!empty($mission)): ?>
        <div id="content-mission"><?php print $mission ?></div>
      <?php endif; ?>

      <?php if (!empty($help)): ?>
        <p id="content-help"><?php print $help ?></p>
      <?php endif; ?>

      <?php if (!empty($messages)): ?>
        <div id="content-message"><?php print $messages ?></div>
      <?php endif; ?>

      <?php print $content; ?>

      <?php if ($content_postfix): ?>
        <div id="content-postfix" class="content postfix"><?php print $content_postfix ?></div>
      <?php endif; ?>
    </div>
  </div>

  <div id="navigation">
    <?php if (!empty($primary_links)): ?>
      <?php print theme('primary_links', $primary_links); ?>
    <?php endif; ?>
    <?php if (!empty($secondary_links)): ?>
      <?php print theme('secondary_links', $secondary_links); ?>
    <?php endif; ?>
    <?php print $breadcrumb; ?>
    <?php print $navigation; ?>
  </div>

  <div id="tools">
    <?php if (!empty($tools)) print $tools ?>
  </div>

  <?php if ($search): ?>
  <div id="search">
    <?php print $search; ?>
  </div>
  <?php endif; ?>

  <div id="siteinfo">
    <?php print $footer ?>
    <?php if ($footer_message): ?>
    <p id="siteinfo-details"><?php print $footer_message;?></p>
    <?php endif; ?>
  </div>
</div>
<?php print $closure;?>
</body>
</html>
