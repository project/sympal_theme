<ul id="nav-section<?php print $id_add ?>" class="horizontal-list">
<?php foreach ($links as $link): ?>
  <li><?php print l($link['title'], $link['href'], $link['attributes']) ?></li>
<?php endforeach; ?>
</ul>