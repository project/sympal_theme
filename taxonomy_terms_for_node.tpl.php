<ul class="tags">
<?php foreach ($links as $link): ?>
  <li><?php print l($link['title'], $link['href'], $link['attributes']) ?></li>
<?php endforeach; ?>
</ul>