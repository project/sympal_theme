<div class="feed-source">
  <?php print $icon ?>
  <?php print $image ?>

  <div class="feed-description"><?php print $description ?></div>
  <div class="feed-url"><em><?php  print t('URL:') ?></em> <?php print $link ?></div>
  <div class="feed-updated"><em><?php print t('Updated:') ?></em><?php print $updated ?></div>
</div>