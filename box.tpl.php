    <div class="content box">
      <?php if ($title): ?>
      <h3 class="content related-content title"><?php print $title ?></h3>
      <?php endif; ?>
      <div class="content related-content body"><?php print $content ?></div>
    </div>
