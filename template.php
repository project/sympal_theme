<?php
/**
 *
 * Sympal theme is a base theme, so it comes with a lot of pre-built code.
 * We aim at consistency, so override almost everything here, and represent it
 * in a consistent way.
 * Feel free to submit new partial files and patches with _phptemplate_callback
 * Bèr Kessels carpentered this in webschuur.com for sympal.nl
 */

/**
 * General core functions
 */

/**
 * menu and navigation functions
 * TODO introduce classes like 'active' and 'expanded'
 */
function sympal_theme_primary_links($links, $id_add = '') {
  if (!empty($id_add)) {
    $id_add = '-'. $id_add;
  }
  return _phptemplate_callback('primary_links', array('links' => $links, 'id_add' => $id_add));
}
function sympal_theme_secondary_links($links, $id_add = '') {
  if (!empty($id_add)) {
    $id_add = '-'. $id_add;
  }
  return _phptemplate_callback('secondary_links', array('links' => $links, 'id_add' => $id_add));
}

function sympal_theme_menu_tree($pid = 1) {
  if ($tree = menu_tree($pid)) {
    // TODO a static var that counts the depth.
    return _phptemplate_callback('menu_tree', array('tree' => $tree, 'pid' => $pid));
  }
}

/**
 * Local tasks AKA tabs
 */
function sympal_theme_menu_local_tasks($id_add = '') {
  $output = '';
  if (!empty($id_add)) {
    $id_add = '-'. $id_add;
  }
  //TODO: build the $links not as an ugly string, but rather an array of items!
  if ($links = menu_primary_local_tasks()) {
    $output = _phptemplate_callback('menu_local_tasks_primary', array('links' => $links, 'id_add' => $id_add));
    $links = '';
  }
  if ($links = menu_secondary_local_tasks()) {
    $output .= _phptemplate_callback('menu_local_tasks_secondary', array('links' => $links, 'id_add' => $id_add));
  }

  return $output;
}

/**
 * A single Localtask AKA tab
 */
function sympal_theme_menu_local_task($mid, $active, $primary) {
  return _phptemplate_callback('menu_local_task', array('mid' => $mid, 'active' => $active));
}
function sympal_theme_menu_item_link($item, $link_item) {
  return _phptemplate_callback('menu_item_link', array('item' => $item, 'link_item' => $link_item));
}

/**
 * Breadcrumbs in a single partial
 */
function sympal_theme_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return _phptemplate_callback('breadcrumb', array('links' => $breadcrumb));
  }
}

/**
 * Comment form
 */
function sympal_theme_comment_form($form) {
  $vars = array(
    'admin' => drupal_render($form['admin']),
    'contact' => drupal_render($form['name']) . drupal_render($form['mail']) . drupal_render($form['homepage']) . drupal_render($form['_author']),
    'subject' => drupal_render($form['subject']),
    'comment' => drupal_render($form['comment_filter']['comment']),
    'filter' => drupal_render($form['comment_filter']['format']),
    'buttons' => drupal_render($form['submit']) . drupal_render($form['preview']),
    'rest' => drupal_render($form),
  );

  return _phptemplate_callback('comment_form', $vars);
}

/**
 * Render a userpicture (avatar)
 *
 * @return string image tag containing the userpicture
 **/
function sympal_theme_user_picture($account) {
  $out = '';
  $vars['account'] = $account;
    
  if (variable_get('user_pictures', 0)) {
    if ($account->picture && file_exists($account->picture)) {
      $vars['picture_file'] = file_create_url($account->picture);
    }
    else if (variable_get('user_picture_default', '')) {
      $vars['picture_file'] = variable_get('user_picture_default', '');
    }

    if (isset($vars['picture_file'])) {
      $vars['alt'] = t("@user's picture", array('@user' => $account->name ? $account->name : variable_get('anonymous', t('Anonymous'))));
      
      if (!empty($account->uid) && user_access('access user profiles')) {
        $vars['has_link'] = TRUE;
      }

      $out = _phptemplate_callback('user_picture', $vars);
    }
  }
  return $out;
}

/**
 * Format a news feed.
 */
function sympal_theme_aggregator_feed($feed) {
  $vars = array(
    'icon' => theme('feed_icon', $feed->url),
    'image' => $feed->image,
    'description' => aggregator_filter_xss($feed->description),
    'url' => l($feed->link, $feed->link, array(), NULL, NULL, TRUE),
  );

  if (user_access('administer news feeds')) {
    $vars['updated'] = l($updated, 'admin/content/aggregator');
  }
  elseif ($feed->checked) {
    $vars['updated'] = t('@time ago', array('@time' => format_interval(time() - $feed->checked)));
  }
  else {
    $vars['updated'] = t('never');
  }

  return _phptemplate_callback('aggregator_feed', $vars);
}

/**
 * Format an individual feed item for display on the aggregator page.
 */
function sympal_theme_aggregator_page_item($item) {

  $vars['source'] = '';
  if ($item->ftitle && $item->fid) {
    $vars['source'] = l($item->ftitle, "aggregator/sources/$item->fid", array('class' => 'feed-item-source'));
  }

  if (date('Ymd', $item->timestamp) == date('Ymd')) {
    $vars['source_date'] = t('%ago ago', array('%ago' => format_interval(time() - $item->timestamp)));
  }
  else {
    $vars['source_date'] = format_date($item->timestamp, 'custom', variable_get('date_format_medium', 'D, m/d/Y - H:i'));
  }

  $vars['description'] = aggregator_filter_xss($item->description);
  $vars['link'] = check_url($item->link);
  $vars['title'] = check_plain($item->title);

  $result = db_query('SELECT c.title, c.cid FROM {aggregator_category_item} ci LEFT JOIN {aggregator_category} c ON ci.cid = c.cid WHERE ci.iid = %d ORDER BY c.title', $item->iid);
  $categories = array();
  while ($category = db_fetch_object($result)) {
    $vars['categories'][] = l($category->title, 'aggregator/categories/'. $category->cid);
  }

  return _phptemplate_callback('aggregator_page_item', $vars);
}

/**
 * Return a themed item heading for summary pages located at "aggregator/sources"
 * and "aggregator/categories".
 *
 * @param $item The item object from the aggregator module.
 */
function sympal_theme_aggregator_summary_item($item) {
  $vars = array(
    'item' => $item,
    'item_link' => check_url($item->link),
    'feed_link' => check_url($item->feed_link),
    'item_title' => check_plain($item->title),
    'feed_title' => check_plain($item->feed_title),
    'age' => t('%age old', array('%age' => format_interval(time() - $item->timestamp))),
  );

  return _phptemplate_callback('aggregator_summary_item', $vars);
}

/**
 * Search form functions
 */
function sympal_theme_search_theme_form($form) {
  return _phptemplate_callback('search_form', array('form' => $form));
}
function sympal_theme_search_block_form($form) {
  return _phptemplate_callback('search_form', array('form' => $form));
}

/**
 * Warning, error and help messages
 */
function sympal_theme_status_messages() {
  if ($data = drupal_get_messages()) {
    foreach ($data as $type => $messages) {
      $grouped_messages .= _phptemplate_callback('status_messages_by_type', array(
        'messages' => $messages,
        'type' => $type,
      ));
    }
    return _phptemplate_callback('status_messages', array(
      'grouped_messages' => $grouped_messages,
    ));
  }
}

/**
 * New functions introduced here, used as helpers or simpler ways to theme stuff.
 */
/**
 * Terms are now themed trough a way too general theme_links.
 * This function is a helper to render terms in a node.
 */
function sympal_theme_taxonomy_terms_for_node($node) {
  $links = taxonomy_link('taxonomy terms', $node);
  if (count($links)) {
    return _phptemplate_callback('taxonomy_terms_for_node', array('links' => $links, 'terms' => $node->taxonomy, 'node' => $node));
  }
  else {
    return '';
  }
}

/**
 * General phptemplate stuff
 */
/**
 * Make additional variables available and override some core variables
 * TODO: add node counters and zebras
 * TODO: add comment counters and zebras
 */
function _phptemplate_variables($hook, $vars) {
  static $count;
  $count = is_array($count) ? $count : array();
  $count[$hook] = is_int($count[$hook]) ? $count[$hook] : 1;
  $vars['zebra'] = ($count[$hook] % 2) ? 'odd' : 'even';
  $vars['seqid'] = $count[$hook]++;

  switch ($hook) {
    case 'page':
      $vars['css'] = _sympal_theme_unset_css($vars['css']);
      $vars['styles'] = drupal_get_css($vars['css']);
    break;
    case 'node':
      $vars['terms'] = theme('taxonomy_terms_for_node', $vars['node']);
    break;
  }

  return $vars;
}

/**
 * Unset all module and core styles
 **/
function _sympal_theme_unset_css($css) {
  if (is_array($css['all']['module'])) {
    $css['all']['module'] = array();
  }
  return $css;
}

/**
 * define regions
 **/
function sympal_theme_regions() {
  return array(
    'content' => t('content'),
    'branding' => t('branding'),
    'navigation' => t('navigation'),
    'tools' => t('tools'),
    'search' => t('search'),
    'siteinfo' => t('siteinfo'),
    'content_prefix' => t('before content'),
    'content_postfix' => t('after content'),
  );
}
?>