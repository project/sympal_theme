<div class="picture">
  <?php if ($has_link): ?>
    <?php print  l(theme('image', $picture_file, $alt, $alt, '', FALSE), "user/$account->uid", array('title' => t('View user profile.')), NULL, NULL, FALSE, TRUE); ?>
  <?php else: ?>
    <?php print theme('image', $file, $alt, $alt, '', FALSE); ?>
  <?php endif; ?>
</div>