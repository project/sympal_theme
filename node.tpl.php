<div class="article<?php print " $type"; print ($sticky ? " sticky" : NULL); print (!$status ? " unpublished" : NULL); ?>">
<div class="sliding-door">
  <?php if ($page == 0): ?>
    <h3 class="title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h3>
  <?php endif; ?>
  <?php if ($terms): ?>
    <div class="tags sliding-door">
      <?php print $terms; ?>
    </div>
  <?php endif; ?>

  <div class="body">
    <?php print $content; ?>
  </div>

  <?php if ($links): ?>
    <div class="links"><?php print $links; ?></div>
  <?php endif; ?>
</div>
</div>
