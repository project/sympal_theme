    <div id="content-<?php print $block->module .'-'. $block->delta; ?>" class="related-content wrapper">
      <?php if ($block->subject): ?>
      <h3 class="content related-content title"><?php print $block->subject ?></h3>
      <?php endif; ?>
      <div class="content related-content body"><?php print $block->content ?></div>
    </div>
