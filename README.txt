## Readme for Sympal Theme

We removed the HEAD 'branch' Development is continued in the 5.x branch.
By the time people are ready for the 6.x branch we will merge the 5.x back in to head. 

## credits
By Bèr Kessels, webschuur.com ber@webschuur.com
For webschuur, Sympal and myself.

First released for Drupal 4.7
[Homepage](http://drupal.org/projects)
                                     _         _
     ___ _   _ _ __ ___  _ __   __ _| |  _ __ | |
    / __| | | | '_ ` _ \| '_ \ / _` | | | '_ \| |
    \__ \ |_| | | | | | | |_) | (_| | |_| | | | |
    |___/\__, |_| |_| |_| .__/ \__,_|_(_)_| |_|_|
         |___/          |_|

 -readme written in markdown-