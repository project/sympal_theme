<?php
/**
 * @file Image module theme calls
 */

/**
 * Theme a teaser
 */
function sympal_theme_image_teaser($node) {
  return _phptemplate_callback('image_teaser', array(
    'node' => $node,
  ),
  array('contribs/image/image_teaser'));
}

/**
 * Theme a body
 */
function sympal_theme_image_body($node, $size) {
  return image_display($node, $size);
}

/**
  * Theme an img tag for displaying the image.
  */
function sympal_theme_image_display($node, $label, $url, $attributes) {
  return _phptemplate_callback('image_display', array(
    'node' => $node,
    'label' => $label,
    'url' => $url,
    'attributes' => $attributes,
  ),
  array('contribs/image/image_display'));
}

/**
 * Image attach: contrib in image module
 * Theme the teaser
 */
function sympal_theme_image_attach_teaser($node){
  drupal_add_css(drupal_get_path('module', 'image_attach') .'/image_attach.css');

  $image = node_load($node->iid);
  $info = image_get_info(file_create_path($image->images['thumbnail']));

  $image_str = l(image_display($image, 'thumbnail'), "node/$node->nid", array(), NULL, NULL, FALSE, TRUE);
 
  return _phptemplate_callback('image_attach_teaser', array(
    'image_str' => $image_str,
    'image' => $image,
    'info' => $info,
  ),
  array('contribs/image/image_attach_teaser'));
}

/**
 * Image attach: contrib in image module
 * Theme the body 
 */
function sympal_theme_image_attach_body($node) {
  drupal_add_css(drupal_get_path('module', 'image_attach') .'/image_attach.css');

  $image = node_load($node->iid);
  $info = image_get_info(file_create_path($image->images['thumbnail']));

  $image_str = l(image_display($image, 'preview'), "node/$node->iid", array(), NULL, NULL, FALSE, TRUE);
 
  return _phptemplate_callback('image_attach_body', array(
    'image_str' => $image_str,
    'image' => $image,
    'info' => $info,
  ),
  array('contribs/image/image_attach_body'));
}