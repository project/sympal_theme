<div class="image sliding-door">
  <img
    class="content image"
    src="<?php print check_url($url) ?>"
    alt="<?php print check_plain($node->title) ?>"
    title="<?php print check_plain($node->title) ?>"
    <?php print drupal_attributes($attributes) ?> />
</div>